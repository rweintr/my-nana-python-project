def validate_input(num_hours):
    try:
        num_hours=int(num_hours)
        if num_hours > 0:
            return True
        elif num_hours == 0:
            print("You entered a zero")
        else:
            print("You entered a negative")
    except ValueError:
        print("You entered an invalid value")

def convert_hrs_to_units(hrs,units):
    hrs=int(hrs)
    if units == "minutes":
        return hrs*60
    elif units == "seconds":
        return  hrs*60*60
    elif units == "milliseconds":
        return hrs*60*60*1000
    elif units == "days":
        return round(hrs/24,3)
    else:
        return "conversion not supported"

user_inp_msg="Enter num of hrs and conversion units (exit to end):\n"