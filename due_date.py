import datetime

user_info=input("Enter project:due date (mm/yy/dddd)\n")
user_info_list=user_info.split(":")
project=user_info_list[0]
due_date=user_info_list[1]
due_date_dt=datetime.datetime.strptime(due_date,"%m/%d/%Y")
curr_date=datetime.datetime.now()
days_to_due_dt=due_date_dt-curr_date
days_to_due_dt=days_to_due_dt.days
print(f"you have {days_to_due_dt} days until {project} is due on {due_date}")