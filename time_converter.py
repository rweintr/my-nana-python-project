from helper import convert_hrs_to_units, validate_input, user_inp_msg
while True:
    hours_and_units=input(user_inp_msg)
    if hours_and_units == "exit":
        break
    hours_units_list=hours_and_units.split(",")
    hours_units_dict={"hours":hours_units_list[0],"units":hours_units_list[1]}
    hours=hours_units_dict["hours"]
    units=hours_units_dict["units"]
    if validate_input(hours):
        result=convert_hrs_to_units(hours,units)
        print(f"{hours} hours is {result} {units}")
