import openpyxl

inventory_file=openpyxl.load_workbook('inventory.xlsx')
inventory_sheet=inventory_file["Sheet1"]

prods_per_supplier={}
inv_value_per_supplier={}
prods_vol_lt_threshold=[]

INVENTORY_THRESHOLD=200

# calculate products and inventory value per supplier
for prod_row in range(2,inventory_sheet.max_row+1):
    supplier_name=inventory_sheet.cell(prod_row,4).value
    inventory_units=inventory_sheet.cell(prod_row,2).value
    prod_no=inventory_sheet.cell(prod_row,1).value
    price=inventory_sheet.cell(prod_row,3).value
    inv_value=inventory_units*price

    if supplier_name in prods_per_supplier:
        prods_per_supplier[supplier_name]=prods_per_supplier.get(supplier_name)+1
        inv_value_per_supplier[supplier_name]=inv_value_per_supplier.get(supplier_name) + inv_value
    else:
        prods_per_supplier[supplier_name]=1
        inv_value_per_supplier[supplier_name]=inv_value

    if inventory_units < INVENTORY_THRESHOLD:
        prods_vol_lt_threshold.append(str(int(prod_no)))

    #add inv value to col 5
    inventory_sheet.cell(prod_row,5).value=inv_value

inventory_file.save("updated_inventory.xlsx")
print(prods_per_supplier)
print(inv_value_per_supplier)
print(prods_vol_lt_threshold)